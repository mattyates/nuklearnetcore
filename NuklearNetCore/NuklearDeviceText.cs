﻿namespace NuklearNetCore;

public abstract unsafe class NuklearDeviceTex<T> : NuklearDevice {
    List<T> Textures;

    public NuklearDeviceTex() {
        Textures = new List<T>();
        Textures.Add(default(T)); // Start indices at 1
    }

    public int CreateTextureHandle(T Tex) {
        Textures.Add(Tex);
        return Textures.Count - 1;
    }

    public T GetTexture(int Handle) {
        return Textures[Handle];
    }

    public sealed override int CreateTextureHandle(int W, int H, IntPtr Data) {
        T Tex = CreateTexture(W, H, Data);
        return CreateTextureHandle(Tex);
    }

    public sealed override void Render(NkHandle Userdata, int Texture, NkRect ClipRect, uint Offset, uint Count) {
        Render(Userdata, GetTexture(Texture), ClipRect, Offset, Count);
    }

    public abstract T CreateTexture(int W, int H, IntPtr Data);

    public abstract void Render(NkHandle Userdata, T Texture, NkRect ClipRect, uint Offset, uint Count);
}
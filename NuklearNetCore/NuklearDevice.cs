﻿namespace NuklearNetCore;

public abstract unsafe class NuklearDevice {
    internal Queue<NuklearEvent> Events;

    public virtual bool EnableFrameBuffered {
        get {
            return true;
        }
    }

    public abstract void SetBuffer(NkVertex[] VertexBuffer, ushort[] IndexBuffer);
    public abstract void Render(NkHandle Userdata, int Texture, NkRect ClipRect, uint Offset, uint Count);
    public abstract int CreateTextureHandle(int W, int H, IntPtr Data);

    public NuklearDevice() {
        Events = new Queue<NuklearEvent>();
        ForceUpdate();
    }

    public virtual void Init() {
    }

    public virtual void FontStash(IntPtr Atlas) {
    }

    public virtual void BeginRender() {
    }

    public virtual void EndRender() {
    }

    public void OnMouseButton(NuklearEvent.MouseButton MouseButton, int X, int Y, bool Down) {
        Events.Enqueue(new NuklearEvent() { EvtType = NuklearEvent.EventType.MouseButton, MButton = MouseButton, X = X, Y = Y, Down = Down });
    }

    public void OnMouseMove(int X, int Y) {
        Events.Enqueue(new NuklearEvent() { EvtType = NuklearEvent.EventType.MouseMove, X = X, Y = Y });
    }

    public void OnScroll(float ScrollX, float ScrollY) {
        Events.Enqueue(new NuklearEvent() { EvtType = NuklearEvent.EventType.Scroll, ScrollX = ScrollX, ScrollY = ScrollY });
    }

    public void OnText(string Txt) {
        Events.Enqueue(new NuklearEvent() { EvtType = NuklearEvent.EventType.Text, Text = Txt });
    }

    public void OnKey(NkKeys Key, bool Down) {
        Events.Enqueue(new NuklearEvent() { EvtType = NuklearEvent.EventType.KeyboardKey, Key = Key, Down = Down });
    }

    public void ForceUpdate() {
        Events.Enqueue(new NuklearEvent() { EvtType = NuklearEvent.EventType.ForceUpdate });
    }
}
﻿namespace NuklearNetCore;

public struct NuklearEvent {
    public enum EventType {
        MouseButton,
        MouseMove,
        Scroll,
        Text,
        KeyboardKey,
        ForceUpdate
    }

    public enum MouseButton {
        Left, Middle, Right
    }

    public EventType EvtType;
    public MouseButton MButton;
    public NkKeys Key;
    public int X, Y;
    public bool Down;
    public float ScrollX, ScrollY;
    public string Text;
}
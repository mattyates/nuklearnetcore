﻿using System.Runtime.InteropServices;

namespace NuklearNetCore;

[StructLayout(LayoutKind.Sequential)]
public struct NkVertex {
    public NkVector2f Position;
    public NkVector2f UV;
    public NkColor Color;

    public override string ToString() {
        return string.Format("Position: {0}; UV: {1}; Color: {2}", Position, UV, Color);
    }
}
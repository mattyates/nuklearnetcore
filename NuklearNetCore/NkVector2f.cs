﻿using System.Numerics;
using System.Runtime.InteropServices;

namespace NuklearNetCore;

[StructLayout(LayoutKind.Sequential)]
public struct NkVector2f {
    public float X, Y;

    public NkVector2f(float X, float Y) {
        this.X = X;
        this.Y = Y;
    }

    public override string ToString() {
        return string.Format("({0}, {1})", X, Y);
    }

    public static implicit operator Vector2(NkVector2f V) {
        return new Vector2(V.X, V.Y);
    }

    public static implicit operator NkVector2f(Vector2 V) {
        return new NkVector2f(V.X, V.Y);
    }
}
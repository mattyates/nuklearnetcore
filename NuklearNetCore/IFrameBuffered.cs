﻿namespace NuklearNetCore;

public interface IFrameBuffered {
    void BeginBuffering();
    void EndBuffering();
    void RenderFinal();
}
﻿using System;
using NuklearNetCore;
using OpenTK.Graphics.OpenGL;
using SFML.Graphics;
using SFML.System;
using Color = SFML.Graphics.Color;
using Image = SFML.Graphics.Image;
using PrimitiveType = SFML.Graphics.PrimitiveType;
using RenderTexture = SFML.Graphics.RenderTexture;
using RenderWindow = SFML.Graphics.RenderWindow;
using Sprite = SFML.Graphics.Sprite;
using Vertex = SFML.Graphics.Vertex;

namespace ExampleSFML;

unsafe class SFMLDevice : NuklearDeviceTex<Texture>, IFrameBuffered {
    RenderWindow _rWind;
    RenderTexture _rt;
    Sprite _renderSprite;

    public SFMLDevice(RenderWindow rWind) {
        _rWind = rWind;
        _rt = new RenderTexture(rWind.Size.X, rWind.Size.Y);
        _renderSprite = new Sprite(_rt.Texture);
    }

    public override Texture CreateTexture(int w, int h, IntPtr data) {
        Image I = new Image((uint)w, (uint)h);
        for (int y = 0; y < h; y++)
        for (int x = 0; x < w; x++) {
            NkColor c = ((NkColor*)data)[y * w + x];
            I.SetPixel((uint)x, (uint)y, new Color(c.R, c.G, c.B, c.A));
        }
        Texture T = new Texture(I);
        T.Smooth = true;
        return T;
    }

    public void BeginBuffering() {
        Console.WriteLine("BeginBuffering");
        _rt.Clear(Color.Transparent);
    }

    NkVertex[] _verts;
    ushort[] _inds;

    public override void SetBuffer(NkVertex[] vertexBuffer, ushort[] indexBuffer) {
        _verts = vertexBuffer;
        _inds = indexBuffer;
    }

    public override void Render(NkHandle userdata, Texture texture, NkRect clipRect, uint offset, uint count) {
        Vertex[] sfmlVerts = new Vertex[count];

        for (int i = 0; i < count; i++) {
            NkVertex v = _verts[_inds[offset + i]];
            sfmlVerts[i] = new Vertex(new Vector2f(v.Position.X, v.Position.Y), new Color(v.Color.R, v.Color.G, v.Color.B, v.Color.A), new Vector2f(v.UV.X, v.UV.Y));
        }

        Texture.Bind(texture);

        GL.Enable(EnableCap.ScissorTest);
        GL.Scissor((int)clipRect.X, (int)_rWind.Size.Y - (int)clipRect.Y - (int)clipRect.H, (int)clipRect.W, (int)clipRect.H);

        //RWind.Draw(SfmlVerts, PrimitiveType.Triangles);
        _rt.Draw(sfmlVerts, PrimitiveType.Triangles);

        GL.Disable(EnableCap.ScissorTest);
    }

    public void EndBuffering() {
        _rt.Display();
    }

    public void RenderFinal() {
        _rWind.Draw(_renderSprite);
    }
}
﻿using System;
using System.Diagnostics;
using ExampleShared;
using NuklearNetCore;
using OpenTK.Graphics.OpenGL;
using OpenTK.Windowing.Common;
using OpenTK.Windowing.Desktop;
using SFML.Graphics;
using SFML.Window;

namespace ExampleSFML;

internal class Program {
	private static void OnKey(NuklearDevice dev, KeyEventArgs e, bool down)
	{
		switch (e.Code)
		{
			case Keyboard.Key.LShift:
			case Keyboard.Key.RShift:
				dev.OnKey(NkKeys.Shift, down);
				break;
			case Keyboard.Key.LControl:
			case Keyboard.Key.RControl:
				dev.OnKey(NkKeys.Ctrl, down);
				break;
			case Keyboard.Key.Delete:
				dev.OnKey(NkKeys.Del, down);
				break;
			case Keyboard.Key.Enter:
				dev.OnKey(NkKeys.Enter, down);
				break;
			case Keyboard.Key.Tab:
				dev.OnKey(NkKeys.Tab, down);
				break;
			case Keyboard.Key.Backspace:
				dev.OnKey(NkKeys.Backspace, down);
				break;
			case Keyboard.Key.Up:
				dev.OnKey(NkKeys.Up, down);
				break;
			case Keyboard.Key.Down:
				dev.OnKey(NkKeys.Down, down);
				break;
			case Keyboard.Key.Left:
				dev.OnKey(NkKeys.Left, down);
				break;
			case Keyboard.Key.Right:
				dev.OnKey(NkKeys.Right, down);
				break;
			case Keyboard.Key.Home:
				dev.OnKey(NkKeys.ScrollStart, down);
				break;
			case Keyboard.Key.End:
				dev.OnKey(NkKeys.ScrollEnd, down);
				break;
			case Keyboard.Key.PageDown:
				dev.OnKey(NkKeys.ScrollDown, down);
				break;
			case Keyboard.Key.PageUp:
				dev.OnKey(NkKeys.ScrollUp, down);
				break;
		}
	}

	private static void Main(string[] args)
	{
		// Create throwaway opengl window
		var nativeWindowSettings = new NativeWindowSettings
		{
			Profile = ContextProfile.Any,
			StartFocused = false,
			StartVisible = false
		};
		var gameWindow = new GameWindow(new GameWindowSettings(), nativeWindowSettings);
		gameWindow.IsVisible = false;
		
		
		var sWatch = Stopwatch.StartNew();
		var vMode = new VideoMode(1366, 768);

		var rWind = new RenderWindow(vMode, "Nuklear SFML .NET", Styles.Close);
		rWind.SetKeyRepeatEnabled(true);
		
		GL.Viewport(0, 0, (int)vMode.Width, (int)vMode.Height);

		var dev = new SFMLDevice(rWind);
		rWind.Closed += (_, _) => rWind.Close();
		rWind.MouseButtonPressed += (_, e) => dev.OnMouseButton((NuklearEvent.MouseButton)e.Button, e.X, e.Y, true);
		rWind.MouseButtonReleased += (_, e) => dev.OnMouseButton((NuklearEvent.MouseButton)e.Button, e.X, e.Y, false);
		rWind.MouseMoved += (_, e) => dev.OnMouseMove(e.X, e.Y);
		rWind.MouseWheelScrolled += (_, e) => dev.OnScroll(0, e.Delta);
		rWind.KeyPressed += (_, e) => OnKey(dev, e, true);
		rWind.KeyReleased += (_, e) => OnKey(dev, e, false);
		rWind.TextEntered += (_, e) => dev.OnText(e.Unicode);

		Shared.Init(dev);

		var dt = 0.1f;

		NuklearAPI.QueueForceUpdate();
		while (rWind.IsOpen) {

			rWind.DispatchEvents();
			rWind.Clear(Color.Black);

			Shared.DrawLoop(dt);

			rWind.Display();

			dt = sWatch.ElapsedMilliseconds / 1000.0f;
			sWatch.Restart();
		}

		Environment.Exit(0);
	}
}